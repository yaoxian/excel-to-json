# excel-to-json

#### 介绍
<p>excel转json简易自用版，官网经常录价格表，所以...</p>

#### 软件架构
<i>略略略...</i>

#### 📦 安装教程
- `npm i`

#### 🐶 使用说明
- `cp sourceXlsx ./xx.xlsx`
  - default -> `test.xlsx`
- export
  - `npm run export` ----> `test.xlsx` -> `test.json`
  - `node ./index.js --sourcepath=./xx.xlsx --exportpath=./xx.json`

- 自己格式化下 😂😂😂